# A bunch of dotfiles and nvim config stuff, should probably split this repo into 2 or more different repos

## TODO when setting things up on a new WSL or Linux system
## These should probably become a bash + ansible role/playbook so we can just fire that off and get a configured envionment

- Install Python
  - python3
  - python3-pip
  - python3-venv
- Install Ansible (preferably in a venv)
- Install [Go]
- Install [Rustup]
  - Install Ripgrep `cargo install ripgrep`
- Install a [Nerdfont](https://www.nerdfonts.com/font-downloads) JetBrainsMono Is pretty nice (use the JetBrainsMonoLN Nerd Font Mono variant)
- Install [Starship](https://starship.rs/)
- Install [FZF](https://github.com/junegunn/fzf)
- Install Lua & libluax.y-dev
  - Install which ever version of lua plenary nvim is using (currently 5.1)
- Install [Luarocks](https://luarocks.org/)
- Install [NeoVim](https://neovim.io/)
- Install [TMUX Package Manager (TPM)](https://github.com/tmux-plugins/tpm)

## Repository TODO:
- Split this into a few repos (one for neovim stuff, one for actual system dotfiles for bash, starship, tmux, etc...)
- Get some friggin ansible written so we don't have to do all of the setup tasks manually
- Add .tmux.conf ... So we don't lose it again
