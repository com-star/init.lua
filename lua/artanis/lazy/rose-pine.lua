function ColorMyPencils(color)
    color = color or "rose-pine"
    vim.cmd.colorscheme(color)

    vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
    vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
end

return {
    {
        "folke/tokyonight.nvim",
        config = function()
            require("tokyonight").setup({
                -- congig foes here
                -- or leave empty for default
                style = "storm", -- theme comes in 3 styles, storm, moon, night and day
                transparent = true, -- enable to disable setting bg color
                terminal_color = true, -- config color used when opening a ':terminal' in nvim
                styles = {
                    -- Style to be allpied to diff syntax groups
                    -- value is any valid attr-list value for ':help nvim_set_hl'
                    comments = { italic = true },
                    comments = { italic = true },
                    -- bg styles can be dark, transparent, or normal
                    sidebars = "dark", -- style for sidebars, see below
                    floats = "dark", -- style for floating windows
                },
            })
        end
    },

    {
        "rose-pine/neovim",
        name = "rose-pine",
        config = function()
            require('rose-pine').setup({
                disable_background = true,
                styles = {
                    italic = true,
                },
            })

            vim.cmd("colorscheme rose-pine")

            ColorMyPencils()

        end
    },

}
